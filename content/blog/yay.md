---
title: "Yay AUR helper"
description: "Yay Archlinux package manager"
date: "2025-01-25"
categories: ["Unix"]
tags: [""]
draft: true 
---


# yay tips

## show files of an installed package
> yay -Ql <packagename>

shows all files that have been instaled for one package

## show pakage pkgbuild doc
> yay -Gp <packagename>

usefull to see which architecture is supported
