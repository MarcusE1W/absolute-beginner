---
title: "Rust start"
description: "Notes on exploring Rust"
date: "2023-01-25"
image: 
math:
hidden: false
comments: false
categories: ["Programming"]
tags: ["Rust"]
draft: true
---


# Install

```
pacman -S rustup
rustup install stable
```
