---
title: "Swift on Archlinux"
date: 2023-10-20T19:09:02+01:00
tags: []
draft: true
---

# Install

## Download

tar xf ...

cd sw../usr

## Install

### Dependencies
yay -S ncurses
ln -s libncursesw.so.6 libncurses.so.6

### Package
doas mkdir /usr/lib/swift
doas cp -r ~/Downloads/swift-5.9.1-RELEASE-ubuntu22.04-aarch64/usr/* /usr/lib/swift

cd /usr/bin
doas ln -s /usr/lib/swift/bin/sourcekit-lsp .
doas ln -s /usr/lib/swift/bin/swift .
doas ln -s /usr/lib/swift/bin/swiftc .

echo '/usr/lib/swift/lib/swift/linux' >> "/etc/ld.so.conf.d/swift.conf"

## Emacs Setup

swift-mode
sourcekit-lsp
lsp-sourcekit.el


# Reference

https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=swift-bin


