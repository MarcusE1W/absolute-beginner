---
title: "Purescript"
date: 2023-11-08T23:12:26Z
draft: true
tags: ["programming","web"]
---

# Purescript

## Install

### purs compiler
Not packaged, download latest release `linux-arm64.tar.gz`from repository

npm also did not work in Archlinux, but maybe Debian or openSuse ?

Add `purs` to PATH

### spago package manager

`sudo npm install -g spago@next`

## Learning


https://book.purescript.org/
