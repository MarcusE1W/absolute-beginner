---
title: "Foot terminal"
description: "Foot core function"
date: "2025-01-03"
image: 
math:
hidden: false
comments: false
categories: ["Linux"]
tags: ["Terminal"]
draft: true
---

# Foot terminal

## Jump to URL

`Ctrl-Shift-o`
Then type the letters in front of the link

## Copy text from the screen with search

1. Search for the beginning of the text that is to be copied
- `Ctrl-Shift-r` to start search
- Key in beginning of the search term until the right word is selected
2. Extend marked area until the end of the wanted text is marked
- `Ctrl-w` extends the search area
3. Commit the search result
- `Return` to finish the search

The final text selection is now marked and can be copied.
