---
title: "LKGL"
description: "Lesser known great languages -"
date: "2022-01-25"
categories: ["Programming"]
tags: ["language"]
draft: true
---


# Lesser known great languages

## Oberon

There is Oberon 02 and Oberon 07. The differences are described [here]().

### VOC
https://github.com/vishaps/voc

```
git clone https://github.com/vishaps/voc
cd voc
export CC=clang
make all
```

### OBNC

https://github.com/rsdoiel/obnc
http://miasap.se/obnc/

### Oberon+
TODO check if it compiles on Pinebook

```
qmake ObxIde2.pro
make
```

https://github.com/rochus-keller/Oberon/issues/18


### Oberon GUI System

### Editor support
# Oberon

# OBNC

# Editor
- Micro
Add [this]() file in ~/.config/micro/syntax
- Lite
## Emacs / Spacemacs
Add `oberon` in section `dotspacemacs-additional-packages` directly below the layers in Spac>
Add
```
  ;; config oberon melpa module
  (add-to-list 'auto-mode-alist '("\\.obn\\'" . oberon-mode))
  (autoload 'oberon-mode "oberon" nil t)
  (add-hook 'oberon-mode-hook (lambda () (abbrev-mode t))) ;; optional; change keywords to C>
```

# Useful links
https://rsdoiel.github.io/blog/2020/04/11/Mostly-Oberon.html


## Ada

The open source Ada compiler is called GNAT (GNU NewYork Ada T...)

On Pinebook Ada is available for Debian, OpenSuse and Fedora.
```
apt install gnat
```
### Alire package manager

### HAC

## Nim
### Nimble


## Zig

[Comparison of Zig and Rust](https://expandingman.gitlab.io/tvu-compare/)
```

## Ocaml

There is also the syntax variant Reason based on Ocaml that aims at web development for people who prefer a C or Javascript like botation. The language is still Ocaml,  though.

### Opam package manager

## FSharp

## Erlang

## Pony

## Odin
http://odin-lang.org/docs/install/

git clone ...
make
odin run hellope.odin

https://github.com/odin-lang/Odin/issues/1437

## Mint
https://github.com/mint-lang/mint

## Derw
https://github.com/eeue56/derw
