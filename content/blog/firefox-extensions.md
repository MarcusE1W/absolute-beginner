
+++
title = "Firefox setting"
date = "2021-10-05"
disable_comments = false
categories = ["Pine"]
tags = ["Firefox"]
draft = false
+++

Firefox settings that are useful for a smaller device like the Pinebook or Pinephone based on the the SBC Pine64.

<!--more-->

# Firefox extensions



## Manage resources

| Extension        | Description                                                       |
|------------------|-------------------------------------------------------------------|
| Auto Tab Discard | Reduce resource need for tabs that have not been used for a while |
| NoScript         | Blocks the execution of any Javascript unless allowed           |
| uBlock Origin    | Add blocker that is nice to system resources                      |
|                  |                                                                   |
## Security

| Extension | Description                                                                          |
|-----------|--------------------------------------------------------------------------------------|
| BitWarden | Makes the passwords of BitWarden (open source, free) easily available in the Browser |

## Comfort

| Extension | Description                                                            |
|-----------|------------------------------------------------------------------------|
| Saka key  | Allows to manage many activities in the browser using key-combinations |

# Firefox tuning

## How to change values in about config.

Key `about:config` into search field
Confirm that you want to make changes
1. Enter e.g. browser.sessionhistory.max_total_viewers in the Filter field. 
2. One preference will appear below. 
3. Right click on it and select Modify.
4. Edit value


## Accessibility service

accessibility.force_disabled = 1


## Reference:

https://www.technogadge.com/how-to-reduce-firefox-memory-usage/

## Stored history pages
Reduces number of recently visited pages stored in memory 

`browser.sessionhistory.max_total_viewers`
Default value is -1 which determines number of pages based on RAM. You can change it to a lower value such as 2 (means two pages will be stored). 
I tried 3.
Press OK.

##Reduce hard drive space used for cache
- Enter `browser.cache.disk.capacity` in the Filter field. 
- One preference will appear below. 
- Change the default value with a lower positive integer. Say 4096. 
- Press OK button.

## Reduce max number of pages in your session history
Enter browser.sessionhistory.max_entries in the Filter field. 
One preference will appear below. 
Default value for the integer is 50. 
Change it to a lower value. Say 5. 
Press OK button.

##Reduce number of closed tabs you can undo
Enter browser.sessionstore.max_tabs_undo in the Filter field. One preference will appear below. Default value for the integer is 25. Change it to a lower value. Say 5. Press OK button.

## Reduce number of closed windows you can undo
Enter browser.sessionstore.max_windows_undo in the Filter field. One preference will appear below. Default value for the integer is 3. Change it to a lower value. Say 1. Press OK button.

