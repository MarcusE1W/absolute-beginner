---
---

# How to create your own blog with a signle site generator (SSG)

## Where to start

What kind of blog. -> Tech blog with articles.

Many options
Wordpress, about half of all blogs use wrodpress.many hosts, relatively simple, teams support
Github, Gitlab or Codeberg pages
Commercial blogs: Ghost,
SSG

For an IT person to host on e.g. Gitlab and use a SSG sounds like a bit of fun and still doable.
Next, we have to find a single site generator.

## What SSG to use?

## Creating a blog with Hugo

pros:
- one app, written in Go
- super fast
- a lot of themes available
- extensive documentation, maybe sommetimes hard to find things, but ultimately a lot of docu is a good thing
### Define Design

Requiremnts:
- simple
- little or no javascript
- tags and categories

Layout:


### Chose Template
There are a lot of templates to choose from.
### Create content
Just write. Write a draft and see later if you want to publish it. Don't wait until the blog is set up perfectly, just start writing in the tool of your choice. Whatever you write can be adapted tho your blog later.

The folder structure of a simple Hugo site looks like this:
```
archetype     -- frontmatter template to create initial articles
content       -- the folder with the articles, initially there is only posts, but that depends on your site layout
	posts     -- the articles go here
layouts        -- if you don't use a theme the layout description files go here
static        -- additional artefacts like pictures can go here
public        -- this is where Hugo writes the generated site
themes        -- Themes live here, thhere can be more than one
config.toml   -- The hugo configuration file
```

In go you place your articles as a standard to the folder `content/posts`. Go can create an initial document for you including a template for the frontmatter if you prepared that in the `archetype' folder.

#### Frontmatter
Frontmatter is a part at the beginnig of the article that allowa to define some attributes for the article. In Go it can be YAML or TOML format.
### Go online

Choose where to host.

Run hugo to publish in `public`. 
It works well for the intended idea, but so well when you forget to run it, use another device where Hugo is not installed to write articles, push the articles with the site to Gitlab, but forget to publish first, and so on.
## Creating a blog with Zola

It worked, but it never worked great. I did not spend enough time to learn the ropes. Every now and then the Gitlab CI threw an error that needed investigating and I did not. 

At some point I heared about Zola

pros:
- one app, written in Rust
- super fast
- a lot of themes available

cons:
- more things than in Hugo are hard errors. Articles need to be reviewed and updated.
- documentation is concise, e.g. no template language reference.

Not that diffferent.
Apparently it is esier to create your own blog by hand in Zola. I have not enough experience to judge that. Let's teke it, for now.
I also was never really happy with the provided templates. I wanted simplicity, little or no java script but a say in what colour 

### Define Design

- choose the colour/appearance
- one person access
- no monetarisation
- single language, english

### Chose Template
### Create content
Frontmatter in TOML ?
### Go online

## Creating your own blog template with Zola (part 2)

User these tutorials:

1.
2.

### A general site structure for beginner

<<graphic>>

<<file structure>>

### Links
Zola template for learing:
https://github.com/not-matthias/apollo/
