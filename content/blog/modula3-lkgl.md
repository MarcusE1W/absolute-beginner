+++
title = "Modula3"
description = "Modula3 - installation and getting started"
date = "2022-01-25"
draft = false
[taxonomies]
categories = ["Programming"]
tags = ["Modula3"]
+++

# Modula3 - installation and getting started

## Introduction

Modula3 is a programming language based on Pascal and Modula 2 from Niklaus Wirth. Modula 3 has been developed around ~1992 by DEC with the blessing of Mr. Wirth.

Modula3 is strongly typed, has a powerfull module system and has generics, Object oriented additions as well as build in support for concurrent programming. The additons to the language add some compfort to programming.

It also seems to be an historic oversight that the multi platform GUI library Trestle has not become more well known and popular. 

>The Trestle multi-platform GUI library from beginning of the 90th allows you to write Modula3 programms for X11 and Windows (Win32) with the same code and it all still works. In 1994 that was almost unique. Well worth to explore a bit.

## Installation

There is an up-to-date Modula3 compiler that installs and runs well on the Pinebook with Manjaro: [CM3](https://github.com/modula3/cm3)

A good install guide is in the [Wiki](https://github.com/modula3/cm3/wiki/Getting-Started:-Linux)

For the Pinebook Pro (aarch64) I have successfully used the tar-file for AMD64.
Also note the `--target ARM64_LINUX` parameter when the build script is called. This is needed for Aarch64 systems as decumented [here](https://github.com/modula3/cm3/wiki/Getting-Started:-Linux#aarch64)

The steps to initial install on Manjaro are:
```
curl -LO https://github.com/modula3/cm3/releases/download/d5.11.4/cm3-dist-AMD64_LINUX-d5.11.4.tar.xz
tar xf cm3-dist-AMD64_LINUX-d5.11.4.tar.xz
mkdir build
cd build
../cm3-dist-AMD64_LINUX-d5.11.4/scripts/concierge.py install --prefix $HOME/cm3 --target ARM64_LINUX all
PATH=$HOME/cm3/bin:$PATH
```

Optional, after the build of the release the latest version from Github can be installed:
```
git clone https://github.com/modula3/cm3 cm3-git
mkdir build
cd build
../cm3-git/scripts/concierge.py install --prefix $HOME/cm3 all --target ARM64_LINUX
```

Notes for the install process:
- The final install folder is called `cm3`, that's why the git folder is called `cm3-git` to keep things seperate
- You probaly don't have to install all prerequisites. Some are installed already
- the `build` folder is created on the same level as the `cm3-git` folder

## Some reading

The compile process takes some time on the Pinebook Pro. This is probably a good opportunity to have a look at some of the highlights of the documentation:
- [Modula3 web site](http://modula3.github.io/cm3/)
- A good overview of the language can be found [here](http://modula3.github.io/cm3/tutorial/m3/m3_toc.html). More a reference with examples than a tutorial.
- [Overview of the Standard Library](http://modula3.github.io/cm3/help/interfaces.html). Modula3 is very module oriented, so this is a good start.
- The highlight for me is the GUI library [Trestle](http://modula3.github.io/cm3/src_reports/src-069.pdf). (Some makefiles in this document are in an older version and need small updates like "" added for names.)

The highlight for me is definately the GUI library Trestle. Works perfect on Manjaro with X11. I have not tested it on Windows, but apparently that works as well. For an 30 years old library that is truely remarkable.

Have I mentiond the WYSIWYG GUI design app [FormsVBT](http://modula3.github.io/cm3/src_reports/formsvbt.pdf) already ?

![formsedit](https://i.postimg.cc/Z0kwtSXX/FormsVBT.png)

So, now the install should be completed. The compiler has been installed in `~/cm3`
Add this bin folder to the PATH `export PATH="$HOME/cm3/bin:$PATH"` 

## Getting started with Modula3

### Example 1: Hello World

Before the eample here is the first m3makefile. Some help for the m3mankefile is [here](http://modula3.github.io/cm3/tutorial/m3/m3_3.html#SEC3). For a small emaple it is not strictly neccessary but this is how Modula3 ticks so let's just use it:

> Create a folder `example`
> Create a subfolder `example/src`
> Create the makefile `example/src/m3makefile`
> Create the Modula3 example `example/src/Hello.m3`

m3makefile
```
% HelloTrestle

import ( "ui")

implementation ("Hello")
program ("Hello")
```

Hello.m3
```
MODULE Hello EXPORTS Main;

IMPORT TextVBT, Trestle;

VAR v := TextVBT.New("Hello Trestle on Pinebook");

BEGIN
  Trestle.Install(v);
  Trestle.AwaitDelete(v)
END Hello.
```

In folder `example` use the command `cm3` and the programme will be compiled.
The result is in `ARM64_LINUX/`. Start the example with `./ARM64_LINUX/Hello`.

You get a nice little Window with a Hello message.

![Hello window](https://i.postimg.cc/XqccBCr7/Hello.png)

If you compare the m3makefile to some documentation you will also notice how to update some of the older m3makefiles mentioned there. Often additional "" are needed.

### Example 2: Module concept

Modula3 has it's name from the module concept. Her eis how it works:
Every module has an interface definition in a file with `.i3` ending. The implementation is in a file with `.m3` ending.

> Create another folder `example2`
> Create `example2/src`

Place all files into the  `src/` folder:

The Interface description looks like this: `A.i3`
```
    INTERFACE A; 
    
    PROCEDURE DoIt();
      
    END A.
```

Here is the implementation of the interface: `A.m3`:
```
    MODULE A;
    IMPORT IO;

    PROCEDURE DoIt() = 
      BEGIN 
          IO.Put("Hello World\n"); 
      END DoIt; 
      
    BEGIN
    END A.
```

And the main programe: `Example2.m3`:
```
    MODULE Example2 EXPORTS Main; 
    IMPORT A; 
    
    BEGIN 
      A.DoIt(); 
    END Example2.
```

All brought together with the `m3makefile`

```
import ("libm3")
implementation ("Example2")
module ("A")
program ("test2")
```

Although some of the source material and documentation is old it still works nicely and Modula3 is even by todays standards a very useful, flexible and pleasant language. The kink with the capitalised KEYWORDS is sorted in the editor, e.g. [Emacs](https://github.com/modula3/cm3/tree/master/m3-tools/gnuemacs/src) or VSCode. You don't need to actually types like that but it makes the structure of the source code very clear and readable.

## Appendix
### Emacs support
The file to support Emacs can be found [here](https://github.com/modula3/cm3/blob/master/m3-tools/gnuemacs/src/modula3.el) in folder []https://github.com/modula3/cm3/tree/master/m3-tools/gnuemacs/src)

Add this to your init.el:
```
;; Path to cm3 modula3 mode
(add-to-list 'load-path "/home/mmw/.local/cm3-git/m3-tools/gnuemacs/src") ;; update to your path

(autoload 'modula-3-mode "modula3")
        (setq auto-mode-alist 
             (append '(("\\.ig$" . modula-3-mode)
                       ("\\.mg$" . modula-3-mode)
                       ("\\.i3$" . modula-3-mode)
                       ("\\.m3$" . modula-3-mode))
                     auto-mode-alist))

(setq completion-ignored-extensions
       (append '(".mo" ".mx" ".mc" ".io" ".ix") completion-ignored-extensions))
```

### Language server
https://github.com/modula3/cm3/tree/master/m3-tools/langserver

### Kate editor support
https://github.com/modula3/cm3/tree/master/m3-tools/kate

### VSCode
Next to the language server, there is a package for syntax highlighting:
https://github.com/newgrammars/vsce-cm3vsc
