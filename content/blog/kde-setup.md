---
title: "Kde Setup"
date: 2023-11-08T20:46:47Z
draft: false
tags: ["KDE"]
---

# My KDE setup

## Behaviour
- 4 Desktops
- click to open


## Shortcuts
  * Desktopt movement
  * Expose
  * Window movement center, etc
  
## Keyboard
make Caps-Lock an additional menu key
  
## Appearance
- Theme Breeze twilight

## Panel settings
To change the panel settings right klick somewhere and choose `Enter Edit mode`

- Edit Mode -> More options -> floating (standardd in Plasma 6)
To get the full more options menu press the buttone `More Options` twice. the first time only a small thin frame appears.
- right side
- height 90% -> drag on top and bottom handles


### Notification
Hide after 3 seconds

## Kwin
- Ctrl-F for toggle title and frame
- Ctrl-C to center windows
