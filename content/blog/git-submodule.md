---
title: "Git Submodule"
cover: "git.jpg"
date: 2020-04-14
categories: ["System"]
tags: ["git", "Shell"]
draft: true
---

How to update a git submodule

<!--more-->


1. git submodule init themes/hugo-notepadium
2. git submodule update --remote themes/hugo-notepadium

