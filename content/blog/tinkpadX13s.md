+++
title = "Linux on ARM Thinkpad X13s"
description = "Thinkpad X13s with Ubuntu 24.10 and other distros"
date = "2025-02-25"
draft = false
[taxonomies]
categories = ["Hardware"]
tags = ["Laptop","Linux"]
+++

# Linux on the Tinkpad X13s

The Thinkpad X13s is still an unusual Linux laptop in that it uses an ARM Snapdragon 8cx processor.
It came out in 2022 and is discontinued by Lenovo so ebay or alternatives are your friend. Mine was around £400.Since then the Snapdragon X Plus and Elite processors where released and it seems the Elite version is catching up with Apple Silicone in performance. The Sanpdragon 8cx in the Tinkpad X13s is not as fast, but compared to almost all other ARM laptops or SBCs it feels snappy. 

The frame of the X13s is made of magnesium and has a nice feel to it. It is quite a bit smaller than a Pinebook Pro which has a 14 inch display and still a little bit smaller than a a Macbook Air with roughly the same display size. It's also a bit lighter than both, although that probably does not make that much of a difference. They are all very portable.

You cannot extend the memory, but you can change the SSD, although Lenovo uses a bit less common standard. You can also unscrew the back for repairs. In the manual is some guidance of what you can repair yourself and what could be done by an expert.

## Display
The display is great. Sharp and without reflection. What you would call a matt display.
The display has a 16:10 form factor. This makes is a bit more square than the 16:9 form that is quite common. This feels good.
The brightness is 300 nit, not the brightest.

## Keyboard and Track-pad
Keyboard feeling is of course very subjective. I like the keyboard. It feels tight, has a certain hub, a bit more than a Macbook Air or HP Elitebook for example. In the end it's probably a normal Thinkpad keyboard. 

The X13s comes with a curious key for Print Screen between the right AltGr and right Ctrl. I first thought there should be some better use case for that key-space, but then nothing much jumped to mind, so it's ok.

I like that the cursor keys are relatively big for a laptop and there are dedicated PageUp and PageDown keys. Home and End key are there but on the top of the keyboard and more difficult to reach, I use Fn-Left/Right instead.

The trackpad has a good size and feels smooth. There is also the usual Thinkpad TrackPoint stick and it works, I just don't use it. I am a trackpad person.

## Battery
The battery of this preloved Thinkpad charges quickly.
It lasts no where near the advertised 25 hours, more like 8-12, as a first guess. My understanding is that sleep is currently not yet implemented in the most efficient way. 
In the last days the battery life was good, not breathtaking but very useful.

## Performance

I think the performance is good for an ARM laptop. Below Apple Silicone or the new Snapdragon X Elite, but quite fast compared to any other ARM SBC or ARM laptop that I know. Significantly faster than the Raspberry Pi 5, for example.

Here are a few Geekbench figures:
| Model/Processor               | single core | multi core |
|-------------------------------|-------------|------------|
| Thinkpad X13s, Snapdragon 8cx | ~1600       | ~6700      |
| Macbook Air M1                | ~2300       | ~8400      |
| Snapdragon X Elite            | ~2600       | ~14200     |
| Raspberry Pi 5                | ~890        | ~2100      |
| Pinebook Pro, RK3399          | ~300        | ~770       |
| RK3588                        | ~800        | ~2500      |

The prublished Geekbench figures are not always consistent and vary but it gives a ballpark where the performance is.

I can say the Tinkpad X13s is fast enough, for me. I come from a Pinebook Pro, the Thinkpad X13s feels fast. In the end it depends on your requirements.

## Ubuntu 24.10

Ubuntu is the first distro that includes X13s support in their normal install iso and it works without further config. Download, burn on a flashdrive, boot, works.
Great job, here.

### What works ?
- Wifi
- Grafik acceleration
- Some special keys for brightness and volume, keyboard light
- lock key for web cam ? TODO:
- Bluetooth works fine. My headphones connected without a problem.
- Ubuntu comes with Gnome 4.7 and works smoothly out of the box. I later installed KDE Plasma 6.1.5 and that also works just fine.
- As a game I tried 0AD. It works very well, you do feel though how the processor gets hot in the top left corner.
- Camera / Webcam: There are several discussions how to get things to work, for me qcam worked after a bit of configuration (see discussion below) and I did not try more.
- The fingerprint scanner could be set up in Gnome. It does show up in `lsusb` and there is a menu option in _KDE Plasma 6 -> settings -> users_, however, I could not enroll a finger in Plasma.  [Additional config](https://wiki.archlinux.org/title/SDDM#Using_a_fingerprint_reader) is needed to get it working with SDDM for the login screen.

### What is still work in progress ?
- Video acceleration (apparently)
- Audio speaker volume is low
This is about how the speakers are designed. To function properly they need some dsp support driver that is not provided (yet?) in Linux. This seems to happen with more and more laptops.

See this [discussion](https://discourse.ubuntu.com/t/status-of-ubuntu-support-for-lenovo-thinkpad-x13s/44652/177) for the Ubuntu support status. The later comments are more relevant.

## Other Linux distributions
Quite a lot of Linux distributions support the Thinkpad X13s by now, in varying levels of completeness.

- [openSuse](https://en.opensuse.org/HCL:ThinkpadX13s)
Install is not out of the box, but with the specific instruction in the wiki it works. You will need the updated .dbt file as described. Follow the procedure for the reboot issue. Also to install use the full .iso.
Otherwise the installer guides you through all steps and you end up with the GUI of your choice, in my case KDE Plasma 6. 
Bluetooth does not work, yet. 
On the other side, this time the finger print sensor worked in KDE Plasma 6. (Kernel 6.13.5)

- [Archlinux](https://github.com/ironrobin/archiso-x13s/releases), not tried yet

- [EndevourOS ARM](https://github.com/ironrobin/archiso-x13s/wiki/EndeavourOS-Tutorial) (Archlinux based), not tried yet

- [Void](https://docs.voidlinux.org/installation/guides/arm-devices/thinkpad-x13s.html)
It works, but the installation process is not that straightforward explained. It boots and after a lot of reading I got wifi to work again. This is somehow left out from the install instructions. I installed
the KDE Plasma packages, but SDDM only shows a black display so far. More to investigate. The installation works, but it makes clear how much more comfort an out of the box installer like Ubuntu gives. I'll write a separate post with the steps that I needed o get Voidlinux up.

- [Debian](https://wiki.debian.org/InstallingDebianOn/Thinkpad/X13s), not tried yet

- [Fedora](https://fedoraproject.org/wiki/Thinkpad_X13s), not tried yet

- [Gentoo](https://wiki.gentoo.org/wiki/Lenovo_ThinkPad_X13s), not tried yet

- [PostmarketOS](https://wiki.postmarketos.org/wiki/Lenovo_ThinkPad_X13s_(lenovo-21bx)) not tried yet

## How to install Linux on the Thinkpad in the first place

I had to do with SBCs and SD-cards so long, I was not completely sure anymore how it would work with a USB stick. It is currently reccomended to keep Windows on the laptop as the BIOS update is otherwise difficult. Before or during the installation you have to make space for the Linux partition.

Here is a list of steps:

- get a USB-C memory stick.
- update the BIOS in Windows, currently version 1.63
- Check in which partition you want to install your Linux. Windows probably uses all the disk space with most of the space in C: . You can shrink the C: partition in Windows or Linux. In Windows I had errors that unmovable files prevented the shrinking. During the Ubuntu installation there was no problem and Windows worked after that just fine with less space in C: . Depending on the distro that you want to install this requires attention though.
- BIOS: Turn on Linux support
- BIOS: Turn off Secure Boot
- Download the ISO or image
- use Rufus to burn the ISO to the USB stick, this will erase the data on the USB stick
- power off
- insert USB stick and turn on
- on boot press F12
- choose the USB stick as start medium
- follow the individual instructions for the distro

## Summary
The Thinkpad X13s is a ARM laptop that has good support for Ubuntu already. Opensuse works well and several other distros provide support as well. Most drivers work and for me it is suitable as a daily driver. 
Compared to other Linux ARM laptops the X13s is quite fast and is pleasant to work with. 
The upcoming Snapdragon X Elite laptops look also promising, but until their price falls significantly and Linux support is improved the Thinkpad X13s is a good option as a ARM Linux laptop.
