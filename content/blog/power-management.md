+++
title = "Power management"
date = 2024-12-11
description = "Power management with TLP on Pinebook Pro"
[taxonomies]
categories = ["Linux"]
tags = ["system", "power"]
[extra]
+++


# TLP power management

In one of the recent Manjaro system updates **TLP** was mentioned as an option to **power-profiles-daemon** for the Pinebook Pro.

On the Pinebook Pro the **TLP** power management gives subjectively better battery performance over the . Especially sleep lasts over night. My PBP is one of the earliest (second batch, I believe) and yet with **TLP** the battery performance is great.

The downside is that the PBP user interface feels slightly more sluggish in KDE Plasma. Everything seems to be a bit slower than it used to be. This might be different in a simpler (tiling) window manager. The TLP documentation mentions that their power management suits light users best (writing, etc).
However, the battery performance feels much improved.

It is now a matter for the user to choose if they prefer performance over battery power.

## Install TLP
```
$ sudo pacman -S tlp
```
You will be asked to uninstall **power-profiles-daemon**, say yes.

There is an additional package `tlp-rdw`, check in the docu if you want that.

To enable the service use (and reboot)
```
$ sudo systemctl enable tlp.service
```

## Config
I have used this config change to improve performance when power is plugged in:
Edit `/etc/tlp.conf` and un-comment and update:
```
CPU_ENERGY_PERF_POLICY_ON_AC=performance
```

If you want you can set `CPU_ENERGY_PERF_POLICY_ON_BAT` to `performance` as well.

You can check the config with: 
```
tlp-stat --config
tlp-stat -p
tlp-stat -p -v
```

Here is a link to the [TLP documentation](https://linrunner.de/tlp/index.html).

# power-profiles-daemon

To switch back to the **power-profiles-daemon**:

## install
``` bash
sudo pacman -S power-profiles-daemon
```

To activate the service (and reboot:
```
sudo systemctl enable power-profiles-daemon
```

And here is the link to the [power-profiles-daemon documentation](https://linuxconfig.org/how-to-manage-power-profiles-over-d-bus-with-power-profiles-daemon-on-linux)
