---
title: "Hyprland on the Pinebook Pro"
description: 
date: 2022-10-18T17:46:30+01:00
image: 
math: 
license: 
hidden: false
comments: true
draft: true
---

# Hyperland on the Pinebook pro

## Installation
Installation is easy with a AUR package that also works with ARM

```
yay -S hyprland-git
```

More tools: Waybar, Kitty, Light, Grim, foot

```
yay -S yay -S waybar-hyprland
yay -S kitty
yay -S light
yay -S grim
yay -S blueberry
```

Kitty seems to be the standard terminal used in the hyperland configuration. I use `alacritty` myself, works as well.
For both Kitty and Alacritty you have to set some environment variables to work with the 3D graphics driver. Yep, they use a 3D graphics driver for a terminal.

## Configuration

### Preperation befor you start

For the terminal Kitty (Alacritty as well, see below) you need to set an evironment variable. I do this by adding this line to `.bash_profile`.

```
export MESA_GL_VERSION_OVERRIDE=3.3 MESA_GLSL_VERSION_OVERRIDE=330 alacritty
```
With this you are good to start. 
Log-out of your current desktop environment and in the display (logon)  manager choose Hyperland as desktop environment. Depending on which display manager you use yoou usually find this selection in one of the corners or in a menu.



### Keyboard settings

Changes for ISO keyboard
```
kb_layout=gb
kb_variant=extd
kb_options= caps:menu
```

The extended `kb_variant` means that you can use other european letters, `AltGr-2 a` for ä, for example. 


### Touchpad settings

### General - active window frame colour

This is completely optional, but I found this border colours for active a nd inactive windows more convenient
```
col.active_border=0xffccffff
col.inactive_border=0x66333333
```

### Windowrules

General rule shema
```
windowrule=RULE,WINDOW
```

Open on workspace 2 a programme of class (if not specified, it's class) firefox  without moving to workplace 2 (silent)
```
windowrule=workspace 2 silent,^(firefox)$
```

[more docu](https://wiki.hyprland.org/Configuring/Window-Rules/)


### Autostart
- Alacrity
- ENV var
- kitty


### Waybar

yay -S waybar-hyprland

Install font
	- config on github has the same symbols without new font

Take std config and modify

move to left, adjust height

#### workspace list

#### Network / Wifi

Mouseclick right and left
Toggle


#### bluetooth

Cheat with "B" to get height right.


#### keyboard

TODO UK example

TODO switch between internal keyboard and e.g bluetooth keyboard


## Bars

### Waybar

### Eww bar
rustup
install

TODO 
TODO setup

### Hybrid-bar

```
cargo install hybrid-bar
```

### Ironbar

```
cargo install ironbar
```

## Additional or optional applications

### Hyprpaper

hypr/hyprpaper.conf: (for std. monitor)
```
preload = ~/Pictures/wallpaper.jpg

wallpaper = ,~/Pictures/wallpaper.jpg

```


```
yay -S hyprpaper-git
```


in hypr/hyprland.conf:
```
exec-once=hyprpaper &
```

### Hyprpicker

```
yay -S hyprpicker-git
```


### Alacritty

```
# export for Alacritty
export MESA_GL_VERSION_OVERRIDE=3.3 MESA_GLSL_VERSION_OVERRIDE=330 alacritty
```

(the same is needed for `Kitty`, just replace alacritty with kitty.

### bluetooth

For bluetooth use `blueberry`

