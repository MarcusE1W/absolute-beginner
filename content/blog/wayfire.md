---
title: "Wayfire with Pinebook Pro"
date: 2022-01-18
categories: ["Desktop Environment"]
tags: ["Wayfire"]
draft: true
---

How to set up Wayfire for the Pinebook Pro

<!--more-->

# Install

- wayfire
- wf-shell
- xorg-xwayland
- wofi  #application menu)
- light #change brightness)
- wlogout #does not work well ?
- grim # for screenshots


(optional) 
wcm       # wayfire configuration manager
xorg-xev  # analyse keypresses (e.g. for configuration)

# Setup

Copy the latest wayfire.ini file from Github into `~/.config`

## Wayfire Config


### Terminal
**NOTE**: Make sure to change the terminal in the config to something that is installed

TODO - touchpad setting


### Wofi
In wayfire.ini file (alternatively also iin wofi config)
> command_launcher = wofi --show=run

### Keyboard layout

Uncomment this:
```
[input]
xkb_layout = us,fr
```
then change `xkb_leyout` to the correct country, e.g. gb (great britain)




Copy /usr/share/doc/wf-shell-git/wf-shell.ini.example  
- background picture


## Window placement
- use numkeys 7-9, U-O, etc

