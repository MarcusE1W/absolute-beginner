---
title: "Qemu"
date: 2023-05-03T20:47:07+01:00
draft: true
---

# Alpinelinux in QEMU

I have used the introduction from [astr0baby](https://forum.pine64.org/showthread.php?tid=8776&highlight=qemu) and a few other internet sources and came up with a working solution to boot Alpinelinux in QEMU on the Pinebook Pro.
The settings are relatively simple and should work with other target ISOs in a similar way. There is no graphics support, yet.
Mind you, this describes how to use qemu for a aarch64 ISO on an aarch64 system.
The host distro used here is Manjaro. There might be some differences with other distros.

Install Qemu and create a folder to keep all files together:

```
pacman -S qemu-base
pacman -S qemu-img
mkdir qemu
```

Copy the ISO of [Alpinelinux](https://www.alpinelinux.org/downloads/) to your qemu folder. 
I have used the Standard ISO for the aarch64 architecture.

Then you need to download a BIOS
```
wget https://releases.linaro.org/components/kernel/uefi-linaro/latest/release/qemu64/QEMU_EFI.fd
```

Last thing to prepare is a virtual harddrive to install Alpinelinux

```
qemu-img create -f qcow2 alpine.qcow2 8G
```

This makes a 8G virtual hard disk in a file named alpine.qcow2, the qcow2 format being a format which appears to be 8G to the guest (VM), but only actually writes to the host any sectors which were written to by the guest in practice. 

## Install Alpinelinux

With all in place you can now start qemu from the .iso file and then install Alpinelinux to your virtual harddrive

```
qemu-system-aarch64 \
-machine virt -cpu cortex-a57 \
-m 1024M -nographic \
-bios QEMU_EFI.fd \
-drive format=raw,readonly=on,file=alpine-standard-3.17.3-aarch64.iso \
-drive file=alpine.qcow2,media=disk,if=virtio
```

A small explanation. The first line is qemu for the aarch64 architecture. The second line describes the cpu.
Then memory of 1024M is assigned. 512M is also fine. No graphics is supported. Next is the bios and then the two drives. The ISO file and the virtual harddrive.

Later I show how to use kvm and smp. They could also be used here, but this keeps is simple.

Once Alpinelinux has booted login like this:
**user: root**
**passwd: <leave empty>**

Once logged in you can straight away start the install process.

```
setup-alpine
```
Please see the [Alpine Docu](https://docs.alpinelinux.org/user-handbook/0.1a/Installing/setup_alpine.html) 
and [Alpine Wiki](https://wiki.alpinelinux.org/wiki/Installation) for details of the installation process.


You will be asked a bunch of questions, most of them will be default.
Things to notice:
- maybe set your timezone
- you will be asked which drive you want to use: vdb
- what kind of installation you want: sys

After the system is installed successfully, you can `poweroff`.
To restart the system you don't need the ISO file anymore.
The command after installation is:

```
qemu-system-aarch64 \
-machine virt -cpu cortex-a57 \
-m 1024M -nographic \
-bios QEMU_EFI.fd \
-drive file=alpine.qcow2,media=disk,if=virtio
```


## SMP
If you want qemu to use more than one cpu add the `-smp 2` switch. This gives two cpus, more is possible.

## KVM

For Kvm to work you either have to add your user in Manjaro to the kvm group, or run qemu with sudo.

Also:
- add -accel kvm
- you can switch `-cpu` to host

If you use KVM there appears to be a bug that sometimes creates this error message:
> qemu-system-aarch64: kvm_init_vcpu: kvm_arch_init_vcpu failed (0): Invalid argument

Just repeat the start command until it works.

```
qemu-system-aarch64 \
-accel kvm -machine virt -cpu host -smp 2 \
-m 1024M -nographic \
-bios QEMU_EFI.fd \
-drive file=alpine.qcow2,media=disk,if=virtio
```

That's it. I am sure there is still much to improve, but it works.


# References
https://drewdevault.com/2018/09/10/Getting-started-with-qemu.html

