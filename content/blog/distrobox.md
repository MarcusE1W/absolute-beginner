---
title: "Distrobox"
date: 2023-07-23T12:52:27+01:00
tags: [linux]
draft: true
---

# Distrobox

## Install distrobox

## Install linux distro

Check which distros are available:

```
distrobox-create --compatibility
```

Examples:

```
distrobox-create --root --name suse --image registry.opensuse.org/opensuse/tumbleweed:latest
distrobox-create --root --name fedora --image fedora:latest
```

**NOTE** Message: .. not found, download -> Yes

## run distro

```
distrobox enter --root suse
```

## Distro info

### Suse

#### package mananger
zypper
